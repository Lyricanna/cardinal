const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name: "time",
    description: "Discord Time Localization Functions",
    type: 'CHAT_INPUT',
    options: [
      {
        name: "current",
        description: "Get Current Server Time, Discord Localized",
        type: "SUB_COMMAND",
      },
      {
        name: "parse",
        description: "Parse a UTC+0 time into a Discord Localized Time.",
        type: "SUB_COMMAND",
        options: [
          {
            name: "year",
            description: "4-digit year",
            required: true,
            type: "NUMBER"
          },
          {
            name: "month",
            description: "month",
            required: true,
            type: "NUMBER",
            choices: [
              {
                name: "JAN",
                value: 0
              },
              {
                name: "FEB",
                value: 1
              },
              {
                name: "MAR",
                value: 2
              },
              {
                name: "APR",
                value: 3
              },
              {
                name: "MAY",
                value: 4
              },
              {
                name: "JUN",
                value: 5
              },
              {
                name: "JUL",
                value: 6
              },
              {
                name: "AUG",
                value: 7
              },
              {
                name: "SEP",
                value: 8
              },
              {
                name: "OCT",
                value: 9
              },
              {
                name: "NOV",
                value: 10
              },
              {
                name: "DEC",
                value: 11
              },
            ]
          },
          {
            name: "day",
            description: "day",
            required: true,
            type: "NUMBER"
          },
          {
            name: "hour",
            description: "hour (use 24-hour time)",
            required: true,
            type: "NUMBER"
          },
          {
            name: "min",
            description: "minutes",
            required: true,
            type: "NUMBER"
          },
        ]
      }
    ],
    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
      const [subcommand, year, month, day, hour, min] = args;
      switch (subcommand) {
        case "current":
          const timeNow = Math.floor(Date.now() / 1000)
          interaction.followUp({ content: `The current time is: <t:${timeNow}:F>` });
          break;
        case "parse":
          const time = new Date();
          time.setUTCFullYear(year);
          time.setUTCMonth(month),
          time.setUTCDate(day | 0);
          time.setUTCHours(hour | 0);
          time.setUTCMinutes(min | 0);
          const timeSeconds =  Math.floor(time.getTime() / 1000)
          interaction.followUp({ content: `The parsed time is: <t:${timeSeconds}:F>\n Copy String:\`\`<t:${timeSeconds}:F>\`\` ` });
          break;
      }

    },
};
