const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name: 'activity',
    description: 'Set CardinalBot\'s current activity,',
    type: "CHAT_INPUT",
    userPermissions: [{
        id: "938607203743580170",
        type: "ROLE",
        permission: true
    }],
    options: [
        {
            name: 'type',
            description: "Activity Type",
            required: true,
            type: "STRING",
            choices: [
                {
                    name: "Playing",
                    value: "PLAYING"
                },
                {
                    name: "Streaming",
                    value: "STREAMING"
                },
                {
                    name: "Listening",
                    value: "LISTENING"
                },
                {
                    name: "Watching",
                    value: "WATCHING"
                }
            ]
        },
        {
            name: "text",
            description: "Activity to state.",
            required: true,
            type: "STRING"
        }
    ],

    run: async (client, interaction, args) => {
        const [type, text] = args;
        client.user.setActivity(text, {type: type});
        interaction.followUp({ content: 'Activity Changed.', ephemeral: true });
    }
};
