const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name : 'server',
    description: 'Replies with Server info.',
    type: 'CHAT_INPUT',
    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
      await interaction.reply(`Server Name: ${interaction.guild.name}\nTotalMembers: ${interaction.guild.memberCount}`);
  }, //end run
}; //end module
