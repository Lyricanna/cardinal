const { MessageActionRow, MessageButton, MessageEmbed } = require('discord.js');

module.exports = {
    name: "button",
    description: 'Creates a button interaction for testing.',
    type: "CHAT_INPUT",
    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
        const row = new MessageActionRow()
			       .addComponents(
				new MessageButton()
				    .setCustomId('primary')
				    .setLabel('Primary')
				    .setStyle('PRIMARY'),
			  );

		    const embed = new MessageEmbed()
			       .setColor('#0099ff')
			       .setTitle('Title Goes Here')
			       .setURL('https://discord.js.org')
			       .setDescription('Some description here');

        await interaction.reply({
            content: 'NepNep!',
            ephemeral: false,
            embeds: [embed],
            components: [row]
        }); //await interaction reply
    }, //end run
};  //end button object
