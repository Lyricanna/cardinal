const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name: "user",
    description: 'Replies with User info.',
    type: 'CHAT_INPUT',
    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
      await interaction.reply(`Your tag: ${interaction.user.tag}\nYour Id: ${interaction.user.id}`);
  }, //end run
};
