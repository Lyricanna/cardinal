const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name: "nep",
    description: "Replies with Nep Nep!",
    type: 'CHAT_INPUT',
    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
        interaction.followUp({ content: "Nep Nep!" });
    }, //end rin
};
