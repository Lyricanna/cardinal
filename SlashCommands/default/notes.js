const { Client, CommandInteraction } = require("discord.js");
const Datastore = require('nedb');

module.exports = {
    name: "note",
    description: "Personal note tracker.",
    type: 'CHAT_INPUT',
    options: [
        {
            name: "add",
            description: "Add note to notes list",
            type: 'SUB_COMMAND',
            options: [
                {
                    name: "text",
                    description: "Text to be posted!",
                    required: true,
                    type: "STRING"
                }
            ]
        }, //end add
        {
            name: "remove",
            description: "Remove last note from notes list",
            type: 'SUB_COMMAND'
        },
        {
            name: "check",
            description: "Check last note from notes list.",
            type: 'SUB_COMMAND'
        }
    ],
    run: async (Cardinal, interaction, args) => {
        const [subcommand, text] = args;
        const user = interaction.user;
        switch (subcommand) {
            case "add":
                Cardinal.memory.users.insert({
                    _id: user.id,
                    username: user.username,
                    campaigns: [],
                    note: [text]
                },(err, newDoc) => {
                    if (err) {
                        Cardinal.memory.users.update({_id: user.id}, {$push: {note: text} }, (error, upDoc) =>{
                            if (error) {
                                console.log(error);
                                interaction.followUp({content: `**Database Error:** See Log`});
                            } else {
                                interaction.followUp({content: `\`\`${text}\`\` added to personal notes.`});
                            }
                        } );
                    } else {
                        interaction.followUp({content: `Notes initalized and \`\`${text}\`\` added to personal notes.`});
                    }
                } ); //end insert
                break;
            case "remove":
                Cardinal.memory.users.update({_id: user.id}, {$pop: {note: 1}  }, (err, numEff, doc) => {
                    if (err) {
                        interaction.followUp({content: `**Database Error:** See Log`});
                    } else {
                        interaction.followUp({content: `Note removed.`});
                    }
                } );
                break;
            case "check":
                Cardinal.memory.users.findOne({_id: user.id}, (err, userData) =>{
                    if (err) {
                        console.log(err);
                        interaction.followUp({content: `**Database Error:** See Log`});
                    } else if (!userData || !userData.note) {
                        interaction.followUp({content: `**Database Error:** Notes Array is Empty`});
                    } else {
                        console.log(userData);
                        const notes = userData.note;
                        interaction.followUp({content: `\`\`${notes[notes.length-1]}\`\` is your most recent note.`});
                    } //end if err
                });
                break;
            default:

        }// end switch
    }
};
