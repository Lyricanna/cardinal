const { Client, CommandInteraction } = require("discord.js");
const axios = require('axios');
const URL_API = "https://uwuaas.herokuapp.com/api/";

module.exports = {
    name: 'uwu',
    description: 'uwuify!',
    type: "CHAT_INPUT",
    options: [
        {
            name: "text",
            description: "text to be uwuified",
            type: "STRING",
            required: true
        }
    ],

    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, args) => {
      //await interaction.deferReply();
      const api_request = {};
      api_request.text = args[0];

      axios.post(URL_API,api_request).then(function (response) {
          const message = {};
          message.content = response.data.text;
          interaction.followUp(message);
      }).catch(function (error) {
          interaction.followUp({content: "uwu a a service is currently offline."});
      });
    },
};
