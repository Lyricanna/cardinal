const { Client, CommandInteraction } = require("discord.js");

module.exports = {
    name: 'post',
    description: 'post to channel',
    type: "CHAT_INPUT",
    userPermissions: [{
        id: "938607203743580170",
        type: "ROLE",
        permission: true
    }],
    options: [
        {
            name: "destination",
            description: "Select a channel",
            required: true,
            type: "CHANNEL"
        },
        {
            name: "text",
            description: "Text to be posted!",
            required: true,
            type: "STRING"
        }
    ],

    run: async (client, interaction, args) => {
console.log(interaction);
        const channel = await interaction.guild.channels.fetch(args[0]);
        const message = args[1];
        await channel.send({content: message} );
        await interaction.followUp({ content: 'Message Posted.', ephemeral: true });

    }, // end run
}; //end repeat module
