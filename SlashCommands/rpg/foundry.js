const { Client, CommandInteraction } = require("discord.js");
const axios = require('axios')

module.exports = {
    name: "foundry",
    description: 'Replies with the status of the foundry server.',
    type: 'CHAT_INPUT',

    /**
     *
     * @param {Client} client
     * @param {CommandInteraction} interaction
     * @param {String[]} args
     */
    run: async (client, interaction, arg) => {
        await interaction.deferReply();

        axios.get('http://localhost:30000').then(response => {
            data = response.data;
            const worlds = [
                {name : 'Skulls & Shackels', id : 'shackels'},
                {name : 'Brands: The Aftermath of War', id : 'brands'},
                {name : 'Grackenburg: The Timeless In-Between', id : 'grack-pf1e' },
                {name : 'Shadowrun Denver', id : 'denver' },
                {name : 'A Simple Job', id: 'A Simple Job' },
                {name : 'Unspecified One-Shot: Molanan', id: 'Adventurer League One-Shots' }
            ];

            if(data.includes('No Active Game') ) {
                output = 'Foundry Server undergoing maintence.\nPlease Wait.';
            } else {
                output = '__Foundry Server Online.__\n**Current World:** ';
            }
            for (const world of worlds) {
                if (data.includes(world.id) ){
                    output = output + world.name;
                }
            } //end for world in worlds
            interaction.followUp({content: output});
        }); //end get-then

    }, //end run
};  //end module
