const Datastore = require('nedb');
const { Client } = require("discord.js");

module.exports = (Cardinal) => {
    Cardinal.memory = {};
    Cardinal.memory.users = new Datastore({filename: './memory/users.db', autoload: true});

};
