const { Client, Collection, Intents } = require("discord.js");

const Cardinal = new Client({
    intents: [Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILDS],
});
module.exports = Cardinal;

// Global Variables
Cardinal.commands = new Collection();
Cardinal.slashCommands = new Collection();
Cardinal.config = require("./token.json");

// Initializing the project
require("./handler")(Cardinal);

Cardinal.login(Cardinal.config.token);

require("./handler/memory.js")(Cardinal);
//Cardinal.guilds.fetch().then(res => {console.log(res)});
