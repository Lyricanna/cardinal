const { SlashCommandBuilder } = require('@discordjs/builders');
const http = require('http')

module.exports = {
  data : new SlashCommandBuilder()
    .setName('uwu')
    .setDescription('uwuify!')
    .addStringOption(option =>
        option.setName('text')
            .setDescription('Text to be uwuified!')
            .setRequired(true) ),

    async execute(interaction) {
      await interaction.deferReply();
      const options = {
        hostname: 'https://uwuaas.herokuapp.com',
        port: 80,
        path: '/api',
        method: 'POST'
      }

      const req = http.request(options, res => {
        res.on('data', async (data) => {
          await interaction.editReply(data.text);
        })
      })

      req.on('error',async (error) => {
        await interaction.editReply("Failure to connect to UwUaaS.");
      })

      req.write(JSON.stringify({
            text: interaction.options.getString('text')
            
        }) );
      req.end()
    },
};
